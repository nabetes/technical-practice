import WordPuzzle from './WordPuzzle.js';
import WordSearcher from './WordSearcher.js';
import cors from 'cors';
import express from 'express';
import jsonwebtoken from 'jsonwebtoken';

const PRIVATE_KEY = 'markov-algorithm-johnnyquesada';
const app = express();
const puzzle = new WordPuzzle();

app.use(express.json());
app.use(cors());

app.get('/api/security/GetToken', (req, res) => {
    res.json(jsonwebtoken.sign({
        Sub: 'Johnny Quesada',
        Birthdate: new Date(1989, 6, 12),
    }, PRIVATE_KEY));
});

app.get('/api/markov/getMatrix', (req, res) => {
    res.json(puzzle.Data);
});

app.get('/api/markov/GetWords', (req, res) => {
    res.json(new WordSearcher(puzzle).FindWords())
});

app.listen(60059, () => console.log('ready'));