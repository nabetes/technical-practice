export default class WordSearcher {
    Puzzle = null;

    PuzzleLengthX = 0;
    PuzzleLengthY = 0;

    constructor(puzzle) {
        this.Puzzle = puzzle;
        this.PuzzleLengthX = this.Puzzle.Data.length;
        this.PuzzleLengthY = this.Puzzle.Data[0].length;
    }


    // represents the array offsets for each
    // character surrounding the current one
    directions =
        [
            { Y: -1,    X: 0 }, // West
            { Y: -1,    X: -1 }, // North West
            { Y: 0,     X: -1 }, // North
            { Y: 1,     X: -1 }, // North East
            { Y: 1,     X: 0 }, // East
            { Y: 1,     X: 1 }, // South East
            { Y: 0,     X: 1 }, // South
            { Y: -1,    X: 1 }, // South West
        ];

    FindWords = () => this.Puzzle.WordsList.reduce((result, word) => {
        const finding = this.Search(word);
        if (finding) result.push(finding);
        return result;
    }, []);

    Search(word) {
        const currentCarater = word[0];
        // scan the puzzle line by line
        for (let row = 0; row < this.PuzzleLengthX; row++) {
            for (let column = 0; column < this.PuzzleLengthY; column++) {
                if (this.Puzzle.Data[row][column] !== currentCarater) continue;

                const finding = this.SearchEachDirection({ chars: word.split(''), column, row });
                if (!finding) continue;

                const { charIndex: end, breakdown } = finding;

                breakdown.unshift({
                    character: currentCarater,
                    row,
                    column,
                });

                return {
                    word,
                    breakdown,
                    start: { column, row },
                    end,
                };
            }
        }

        return null;
    }

    SearchEachDirection({ chars, column, row, breakdown = [] }) {
        const { directions } = this;
        for (let direction = 0; direction < directions.length; direction++) {
            const reference = this.SearchDirection({
                breakdown,
                chars,
                row,
                direction: this.directions[direction],
                column
            });
            if (reference) return reference;
        }

        return null;
    }

    SearchDirection({ chars, column, row, direction, breakdown }) {
        // have we ve moved passed the boundary of the puzzle
        if (column < 0 || row < 0 || column >= this.PuzzleLengthY || row >= this.PuzzleLengthX) return null;
        if (this.Puzzle.Data[row][column] !== chars[0]) return null;
        // when we reach the last character in the word
        // the values of x,y represent location in the
        // puzzle where the word stops
        if (chars.length === 1) return {
            charIndex: { column, row },
            breakdown,
        };

        const { X, Y } = direction;
        const nextRow = row + X;
        const nextColumn = column + Y;
        
        const copy = [...chars];
        copy.shift();

        const result = this.SearchDirection({ chars: copy, row: nextRow, column: nextColumn, direction, breakdown });

        if (result) {
            const { charIndex, breakdown: currentBreakdown } = result;
            return {
                charIndex,
                breakdown: [{
                    character: this.Puzzle.Data[nextRow][nextColumn],
                    column: nextColumn,
                    row: nextRow,
                }, ...currentBreakdown]
            };
        }

        return null;
    }
}
