import fs from 'fs';

export default class WordPuzzle
{
    /* Contains a list with the strings to be solved with Markov Algorithm [*] */
    Inputs = [];

    /*
    * Values: Contains a list which has a set of rules that must be applied to the character list,
    * where “input” is the text that will be deciphered with Markov Algorithm and “constraints”
    * where each node has the following values[***]:
    */
    RuleConfigurationPerInput = [];

    /*
    * Rules: Contains a list with the rules that will be used in Markov Algorithm [**]
    */
    Rules = [];

    /*
    * Words: This file contains a list of the possible Word matches inside the Word puzzle
    */
    WordsList = [];

    /* Data source of the puzzle */
    Data = [];

    /*
    * Contructor of the class
    */
    constructor()
    {
        const rulesJsonString = fs.readFileSync('./assets/base.json');
        const valuesJsonString = fs.readFileSync('./assets/values.json');
        const wordsJsonString = fs.readFileSync('./assets/words.json');
        const cypherString = fs.readFileSync('./assets/cypher.json');

        this.Inputs = JSON.parse(cypherString);
        this.RuleConfigurationPerInput = JSON.parse(valuesJsonString);
        this.Rules = JSON.parse(rulesJsonString);
        this.WordsList = JSON.parse(wordsJsonString);

        this.CreatePuzzle();
    }

    /*
    * Creates the matrix
    */
    CreatePuzzle()
    {
        this.Data = [];
        for (let index = 0, length = this.Inputs.length; index < length; index++)
        {
            this.Data.push(this.markovAlgorithm(this.Inputs[index], this.RuleConfigurationPerInput[index]));
        }            
    }

    /*
    ** method in charge to execute the markov algorithm
    /// <param name="inputValue">Value of the cypher lists</param>
    /// <param name="ruleConfigurationPerInput">Configuration of the different rules that should be applied to the input</param>
    /// <returns>char array that represent the result of the decode</returns>
    */  
    markovAlgorithm(inputValue, ruleConfigurationPerInput)
    {
        let result = inputValue;
        const config = ruleConfigurationPerInput.sort((a, b) => {
            if (a.order > b.order) return 1;
            if (b.order > a.order) return -1;
          
            return 0;
          });
    
        for (let index = 0, length = config.length; index < length; index++)
        {
            const someConfig = config[index];
            const currentRule = this.Rules[someConfig.rule];

            if (result.includes(currentRule.source))
            {
                result = result.split(currentRule.source).join(currentRule.replacement);
                if (someConfig.isTermination)
                {
                    break;
                }
            }
        }

        return result;
    }
}