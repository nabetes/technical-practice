String.prototype.$$stringFormat = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined' ? args[number] : match;
    });
};

String.prototype.$$replaceAll = function (find, replace) {
    var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
};
'use strict';
var $$ = new function $$prototype() {
    this.toArray = function () {
        var source = arguments.length == 1 ? arguments[0] : arguments;
        return Array.prototype.slice.call(source);
    }
};
'use strict';
angular.module('$$exception', [])// only apply for controller         
    .factory("$$exception", ["$rootScope", "$document", "$compile", function ($rootScope, $document, $compile) {
        var modalTemplate = '<div id="SSexception-modal" $$modal="sv.modal" $$title="sv.title"> <div class="modal-body SSexception-modal-list" ng-bind-html="sv.msgExceptions"></div> <footer class="modal-footer"> <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal" ng-bind="sv.labelAccept"> </button> </footer> </div>';
        var genericErrorMessage = "Something unexpected happened, contact the support manager.";
        var $scope = $rootScope.$new();
        window.$$exception = $$exception;
        window.$scope = $scope;

        $scope.sv = {
            msgExceptions: "",
            labelAccept: "Accept",
            title: "System message!",
            modal: null
        };

        /**
        * Constructor of the class
        * @param error: error
        */
        function $$exception() {
            this.Exceptions = [];
            this.message = genericErrorMessage;

            if (arguments.length) {
                $$exception.prototype.add.apply(this, arguments);
            }
        };

        /*
        * Execute when an exception appear
        */
        $$exception.onError = null;

        /**
        * Add new exceptions to the colecction
        * @param error: error
        */
        $$exception.prototype.add = function () {
            var $this = this,
                error = arguments[0];
            switch (typeof error) {
                case "string":
                    if (error.indexOf("ExceptionList") != -1) {
                        var request = JSON.parse(error);
                        $this.message = request.Message;
                        $this.add(request.ExceptionList);
                    } else {
                        if (arguments.length > 1) {
                            Array.prototype.shift.apply(arguments);
                            error = String.prototype.$$stringFormat.apply(error, arguments);
                        } else {
                            $this.message = error;
                            error = genericErrorMessage;
                        }
                        $this._addStringError(error);
                    }
                    break;
                case "function":
                    $this.add(error("$$error"));
                    break;
                case "object":
                    if (error.constructor === $$exception) {
                        $this.Exceptions = $this.Exceptions.concat(error.Exceptions);
                    } else {
                        if (error.message) {
                            $this.message = error.message;
                            $this._addStringError(genericErrorMessage);
                        } else if ($.isArray(error)) {
                            angular.forEach(error, function (message) {
                                $this._addStringError(message);
                            });
                        } else {
                            $this.message = JSON.stringify(error);
                            $this._addStringError(genericErrorMessage);
                        }
                    }
                    break;
            }
            return this;
        };

        /**
         * Add a string exception
         */
        $$exception.prototype._addStringError = function (error) {
            this.Exceptions.push('<span class="SSexception-item"> <span class="SSexception-item-icon zmdi zmdi-alert-circle zmdi-hc-fw"></span>{0}</span>'.$$stringFormat(error));
        }

        /**
        * Check if the handler have exceptions
        */
        $$exception.prototype.validate = function () {
            if (this.Exceptions.length) {
                this.throw();
            }
        };

        /**
        * Add and throw a new exception
        * @param error: error
        */
        $$exception.prototype.addAndThrow = function () {
            $$exception.prototype.add.apply(this, arguments);
            this.throw();
        };

        /**
        * Clean the exceptions
        */
        $$exception.prototype.clean = function () {
            this.Exceptions = [];
        };

        /**
         * throw the current handler
         */
        $$exception.prototype.throw = function () {
            throw this;
        };

        /* Show an exception
        * @param {generic} exception 
        */
        $$exception.prototype.show = function () {
            if (!$scope.sv.modal) {
                $document.find("body:first").append($compile(modalTemplate)($scope));
            }

            if (arguments.length) {
                $$exception.prototype.add.apply(this, arguments);
            }
            $scope.sv.msgExceptions = this.Exceptions.join("");
            this.Exceptions = [];
            if (!$scope.$root.$$phase) {
                $scope.$root.$digest();
            }
            $scope.sv.modal.open();
        }

        /**
         * Add and throw
         * @param {object} parametros del error
         */
        $$exception.addAndThrow = function () {
            $$exception.prototype.addAndThrow.apply(new $$exception(), arguments);
        };

        /* Show an exception
        * @param {generic} exception 
        */
        $$exception.show = function () {
            var exception = arguments[0];
            if (exception.constructor !== $$exception) {
                if (arguments.length > 1) {
                    exception = new $$exception();
                    exception.add.apply(exception, arguments);
                } else {
                    exception = new $$exception(exception);
                }
            }
            exception.show();
        };

        return $$exception;
    }])
    .config(["$provide", "$$exceptionProvider", function ($provide, $exceptionProvider) {
        $provide.decorator('$exceptionHandler', ["$delegate", "$injector", function ($delegate, $injector) {
            var $$exception = null;
            return function (error, cause) {
                if (!$$exception) {
                    $$exception = $injector.get("$$exception");
                }
                $delegate(error, cause);//default error handling
                if ($$exception.onError)
                    return $$exception.onError(error);

                $$exception.show(error);//custom error
            };
        }]);
    }]);
'use strict';
angular.module('$$http', [])
    .factory("$$http", [function () {
        return {
            onRequest: null
        }
    }])
    .config(["$provide", function ($provide) {
        var $$http = null;
        $provide.decorator('$http', ["$delegate", "$injector", function ($delegate, $injector) {
            var $get = $delegate.get,
                $post = $delegate.post;

            if (!$$http) {
                $$http = $injector.get("$$http");
            }

            function execute(args, task) {
                if ($$http.onRequest) {
                    return $$http.onRequest.apply($delegate, [task].concat(args));
                }

                return task.apply($delegate, args);
            }

            $delegate.get = function () {
                return execute([].slice.call(arguments), $get);
            };

            $delegate.post = function () {
                return execute([].slice.call(arguments), $post);
            };

            return $delegate;
        }]);
    }]);
'use strict';
angular.module('$$state', ['ui.router', 'oc.lazyLoad'])
    .provider('$$stateDictionary', function () {
        this.$get = function () {
            return {
                scripts: {},
                modules: {}
            };
        };
    })
    .provider('$$state2', ["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
        var $arguments = [];
        this.$get = ['$ocLazyLoad', '$q', "$$stateDictionary", "$state", function ($ocLazyLoad, $q, $$stateDictionary, $state) {
            return {
                resolve: function () {
                    var $arguments = arguments;
                    return {
                        _modulesAndScripts: function () {
                            var requestsCollection = [];
                            for (var index = 0, length = $arguments.length, config = null, request = null; index < length; index++) {
                                config = $arguments[index];
                                if (typeof config === "string") {
                                    request = $ocLazyLoad.load($$stateDictionary.scripts[config] || $$stateDictionary.modules[config]);
                                } else {
                                    request = $ocLazyLoad.load(config);
                                }
                                requestsCollection.push(request);
                            }
                            return $q.all(requestsCollection);
                        }
                    };
                },
                reload: function () {
                    return $state.reload();
                },
                state: function (name, config) {
                    return $stateProvider.state(name, config);
                },
                otherwise: function (name) {
                    return $urlRouterProvider.otherwise(name);
                },
                go: function () {
                    return $state.go.apply($state, $$.toArray(arguments));
                },
                get: function () {
                    return $state.get.apply($state, $$.toArray(arguments));
                }
            };
        }];
    }]);
"use strict";
angular.module('$$session', [])
    .provider('$$sessionManagerDictionary', function () {
        var dictionary = {};

        this.addMedia = function (key, value) {
            dictionary[key] = value;
        }

        this.$get = function () {
            return dictionary;
        };
    })
    .provider("$$session", [function () {
        var $this = this;
        $this.currentMedia = null;

        $this.$get = ["$q", "$$sessionManagerDictionary", "$window", function ($q, $$sessionManagerDictionary, $window) {
            function onReady() {
                if ($this.currentMedia._promise.$$state.status === 1) {
                    return $q.when(1);
                }

                return $this.currentMedia._promise;
            }

            function setKey(key) {
                $window.localStorage.setItem("$$sessionKey", key || $this.currentMedia.key);
            }

            function getKey() {
                return $window.localStorage.getItem("$$sessionKey");
            }

            var $socialService = {
                _setKey: function (key) {
                    setKey(key);
                },
                _getKey: function (key) {
                    getKey();
                },
                login: function (key, config) {
                    $this.currentMedia = $$sessionManagerDictionary[key];
                    setKey();
                    return onReady().then($this.currentMedia.checkStatus)
                        .then(function (response) {
                            return response;
                        }, function () {
                            return $this.currentMedia.login();
                        });
                },
                logOut: function () {
                    if ($this.currentMedia) {
                        return $this.currentMedia.logOut();
                    }
                    return $q.when(1);
                },
                checkStatus: function (key) {
                    $this.currentMedia = $$sessionManagerDictionary[key];
                    setKey();
                    return $this.currentMedia.checkStatus();
                },
                getUser: function () {
                    return $this.currentMedia.getUser();
                },
                initialize: function () {
                    var statusRequest = null,
                        key = getKey();

                    if (key) {
                        $this.currentMedia = $$sessionManagerDictionary[key];
                        $this.currentMedia._promise = $this.currentMedia.initialize()
                            .then($this.currentMedia.checkStatus);

                        statusRequest = $this.currentMedia._promise;
                    } else {
                        statusRequest = $q.when(1);
                    }

                    return statusRequest.then(function (request) {
                        return request;
                    }, function (request) {
                        var $access = null;
                        for (var access in $$sessionManagerDictionary) {
                            $access = $$sessionManagerDictionary[access];
                            if ($access.key !== key) {
                                $access._promise = $access.initialize();
                            }
                        }
                        return $q.reject(request);
                    });
                }
            };
            return $socialService;
        }];
    }]);
angular.module('$$modal', [])
    .directive("$$modal", ["$parse", function ($parse) {
        return {
            restrict: 'A',
            scope: {
                $$modal: "@",
                $$title: "=?",
                $$size: "@?",
                $$onOpen: "&?"
            },
            transclude: {
                '$$header': '?header',
                '$$footer': '?footer'
            },
            template: '<div id="test1" class="modal fade" tabindex="-1" style="margin-right:10px"> <div class="modal-dialog"> <div class="modal-content"> <ng-transclude ng-if="$$header" ng-transclude-slot="$$header"> </ng-transclude> <div ng-if="$$title" class="modal-header"> <h5 class="modal-title pull-left" ng-bind="$$title"></h5> </div> <ng-transclude> </ng-transclude> <ng-transclude ng-if="$$footer" ng-transclude-slot="$$footer"> </ng-transclude> </div> </div> </div>',
            link: function ($scope, $element, $attrs, $ctrl, $transclude) {
                $scope.$$header = $transclude.isSlotFilled('$$header');
                $scope.$$footer = $transclude.isSlotFilled('$$footer');

                $scope.$$size = $scope.$$size || 'default';
                $scope.$$title = $scope.$$title || 'title';                

                var element = $($element[0].childNodes[0]);

                function $$modal() {
                    if ($attrs.$$modal) {
                        $parse($attrs.$$modal).assign($scope.$parent, this);
                    }
                }

                $$modal.prototype.open = function () {
                    if ($scope.$$onOpen) {
                        $scope.$$onOpen();
                    }
                    if (!$scope.$root.$$phase) {
                        $scope.$digest();
                    }
                    element.modal('show');
                }

                $$modal.prototype.close = function () {
                    element.modal('hide');
                }

                return new $$modal();
            }
        };
    }]);
'use strict';
angular.module('markovPuzzle', ['ui.router', 'oc.lazyLoad', 'ngSanitize', '$$exception', '$$modal', "appRouteConfig"])
    .config(['$ocLazyLoadProvider', "$locationProvider", function ($ocLazyLoadProvider, $locationProvider) {
        $locationProvider.hashPrefix('');

        $ocLazyLoadProvider.config({
            debug: false,
            events: true,
            modules: []
        });
    }]);
'use scrict';
angular.module('$$sessionTest', ['$$session', 'oc.lazyLoad'])
    .provider("$$markovPuzzle", [function () {
        var $this = this;
        this.config = {
            urlBase: "",
            tokenKey: "$$sessionTest"
        };
        this.setConfig = function (config) {
            Object.assign($this.config, config);
        }
        this.$get = function () {
            return {
                config: $this.config
            }
        }
    }])
    .run(["$$sessionManagerDictionary", "$$markovPuzzle", "$ocLazyLoad", "$q", "$http", "$window", function ($$sessionManagerDictionary, $$markovPuzzle, $ocLazyLoad, $q, $http, $window) {
        $$markovPuzzle.getToken = getToken;

        function getToken() {
            return $window.localStorage.getItem($$markovPuzzle.config.tokenKey);
        }

        function setToken(value) {
            $window.localStorage.setItem($$markovPuzzle.config.tokenKey, value);
        }

        var media = {
            key: "markovPuzzle",
            login: function () {
                if (getToken()) {

                    return $q.when(1);
                }
                return this.checkStatus();
            },
            logOut: function () {
                setToken("");
                return $.when(1);
            },
            getUser: function () {
                var token = getToken();
                if (token) {
                    var base64Url = token.split('.')[1];
                    var base64 = base64Url.replace('-', '+').replace('_', '/');
                    return $q.when(JSON.parse(window.atob(base64)));
                }

                return $q.reject(null);
            },
            checkStatus: function () {
                if (getToken()) {
                    return $q.when(1);
                }

                /*In this implementation the "check status" method of this session manager, 
                 * already have get the token of validation, so thats why dont use the login method in any place of the app.
                 */
                return $http.get($$markovPuzzle.config.urlBase + "api/security/GetToken")
                    .then(function (request) {
                        setToken(request.data.token);
                        return request;
                    });
            },
            initialize: function () {
                return $q.when(1);
            }
        };
        $$sessionManagerDictionary.markovPuzzle = media;
    }]);
'use strict';
angular.module('appSessionModule', [])
    .provider('$$session', [function () {
        var currentUserKey = "appUser";
        var $this = this;
        this.appName = "home";
        this.$get = ['$q', '$timeout', "$window", function ($q, $timeout, $window) {
            function validateUser() {
                var currentUser = null;
                currentUser = $window.localStorage.getItem(currentUserKey);
                if (true) {
                    return loadHome();
                }

                if ($this.appName != "login") {
                    location.href = window.location.origin + "/login.html";
                }
            }

            function loadHome() {
                // logic associated with seccion initialization could be here.
            }

            return {
                validate: function () {
                    var mockPromise = $q.defer();
                    $timeout(function () {
                        validateUser();
                        mockPromise.resolve({ name: "johnny" });
                    }, 0);
                    return mockPromise.promise;
                }
            }
        }]
    }]);

'use scrict';
angular.module("appRouteConfig", ["$$http", "$$session", "$$state", "$$sessionTest"])
    .config(["$$markovPuzzleProvider", function ($$markovPuzzleProvider) {
        $$markovPuzzleProvider.setConfig({
            urlBase: "http://localhost:60059/"
        });
    }])
    .run(["$$session", "$$state2", "$$stateDictionary", "$window", "$q", "$$exception", "$$http", "$$markovPuzzle",
        function ($$session, $$state, $$stateDictionary, $window, $q, $$exception, $$http, $$markovPuzzle) {
            $$session.user = null;
            $$session.$$userRequest = $q.defer();
            $$session._setKey("markovPuzzle");

            $$exception.onError = function (error) {
                //check if is controlled exception from de web api, logOut
                $$exception.show(error);
            }

            $$http.onRequest = function () {
                //  if the request if for the the web api, we should add the current token.
                var args = arguments,
                    $delegate = args[0],
                    firstParameter = null;
                Array.prototype.shift.call(args);
                firstParameter = args[0];

                // Having some problems with angular $http headers to oAuth
                /*
                 * If the request its go to markov-algorithm-api, this section should include the validation token,
                 * that .net needs to validate the access to the differents controllers.
                 * 
                 * By now the "authorize attribute" of the controllers are not included,
                 * security wasn't included on the requirements, so because of time I'll escape this part
                 */
       
                return $delegate.apply(null, [].slice.call(args));
            }

            $$session.initialize().then(function (request) {
                if ($window.location.pathname === "/login.html")
                    return $window.location.href = window.location.origin;

                $$stateDictionary.scripts.home = {
                    cache: true,
                    files: ['views/home/home.js?v=0']
                };

                $$state.$resolve = function () {
                    var $argumengs = arguments,
                        $base = $$state.resolve.apply($$state, $$.toArray($argumengs));

                    $base["user"] = ["$q", "$timeout", function ($q, $timeout) {
                        return $$session.user ?
                            $q.when($$session.user) : $$session.getUser().then(function (user) {
                                $$session.user = user;
                                if ($$session.$$userRequest.promise.$$state.status === 0) {
                                    $$session.$$userRequest.resolve(user);
                                }
                                return $$session.user;
                            });
                    }];

                    return $base;
                };

                $$state.otherwise('home');
                $$state.state("home", {
                    url: '/home',
                    templateUrl: 'views/home/home.html',
                    controller: 'homeController',
                    resolve: $$state.$resolve('home')
                });
                $$state.go("home");
            }, function () {
                if ($window.location.pathname !== "/login.html")
                    $window.location.href = window.location.origin + "/login.html";
            });
        }]);
angular.module('markovPuzzle')
    .directive("wordPuzzle", ["$parse", "$http", "$$markovPuzzle", "$timeout",
        function ($parse, $http, $$markovPuzzle, $timeout) {

            function _lightenDarkenColor(color, percent) {
                var usePound = false;

                if (color[0] == "#") {
                    color = color.slice(1);
                    usePound = true;
                }

                var num = parseInt(color, 16);

                var r = (num >> 16) + percent;

                if (r > 255) r = 255;
                else if (r < 0) r = 0;

                var b = ((num >> 8) & 0x00FF) + percent;

                if (b > 255) b = 255;
                else if (b < 0) b = 0;

                var g = (num & 0x0000FF) + percent;

                if (g > 255) g = 255;
                else if (g < 0) g = 0;

                return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
            }

            $(document).on("mouseover.puzzle-word", ".puzzle-word", function () {
                var element = $(this),
                    model = element.data("$scope").item;
                $(model.selector).css({
                    "background-color": _lightenDarkenColor(model.color, - 75),
                    "font-weight": 600,
                    color: "white"
                });
            });

            $(document).on("mouseleave.puzzle-word", ".puzzle-word", function () {
                var element = $(this),
                    model = element.data("$scope").item;
                $(model.selector).css({
                    "background-color": model.color,
                    "font-weight": 200,
                    color: "black"
                });
            });

            return {
                restrict: 'A',
                scope: {
                    puzzleMatrix: "=",
                    puzzleWords: "@"
                },
                transclude: false,
                replace: true,
                template: '<table> <tr ng-repeat="row in puzzleMatrix" class="matrix-row"> <td ng-repeat="item in row" ng-bind="item.v" class="matrix-cell" id="{{item.id}}"></td> </tr> </table>',
                link: function ($scope, $element, $attrs, $ctrl, $transclude) {

                    function puzzle() {
                        this.words = [];
                        if ($attrs.wordPuzzle) {
                            $parse($attrs.wordPuzzle).assign($scope.$parent, this);
                        }
                    }

                    puzzle.prototype.findWords = function () {
                        var $this = this;
                        return $http({
                            method: 'GET',
                            url: $$markovPuzzle.config.urlBase + "api/markov/getWords"
                        }).then(function (request) {
                            $this.words = request.data;
                            return $this._animateFindings();
                        });
                    }

                    puzzle.prototype._animateFindings = function (words, newWords) {
                        var $this = this,
                            _words = [].concat(words || this.words),
                            newWords = newWords || [],
                            currentWord = null;

                        if (_words.length) {
                            currentWord = _words.pop();
                            currentWord.color = $this._getRandColor();
                            currentWord.selector = "";
                            newWords.push(currentWord);

                            if ($attrs.puzzleWords) {
                                $parse($attrs.puzzleWords).assign($scope.$parent, newWords);
                            }

                            currentWord.breakdown.forEach(function (item, index2) {
                                currentWord.selector += "#{0}-{1}, ".$$stringFormat(item.row, item.column);
                            });

                            currentWord.selector = currentWord.selector.substring(0, currentWord.selector.length - 2);
                            $this._updatetyle(currentWord, _words, newWords);
                        }
                    };

                    puzzle.prototype._updatetyle = function (currentWord, _words, newWords) {
                        var $this = this;
                        $timeout(function () {
                            $(currentWord.selector).css("background-color", currentWord.color);
                            $timeout(function () {
                                $this._animateFindings(_words, newWords);
                            }, 300);
                        }, 0);
                    };

                    puzzle.prototype._getRandColor = function (brightness) {
                        var letters = 'BCDEF'.split('');
                        var color = '#';
                        for (var i = 0; i < 6; i++) {
                            color += letters[Math.floor(Math.random() * 4)];
                        }
                        return _lightenDarkenColor(color, -12);
                    };

                    return new puzzle();
                }
            };
        }]);
'use strict';
angular.module("markovPuzzle")
    .controller("indexController", ["$scope", "$$session", "$window", "$q", "$$state2", "$$stateDictionary", "$http", "$$state2",
        function ($scope, $$session, $window, $q, $$state, $$stateDictionary, $http, $$state2) {

            $scope.sv = {
                user: null,
                appSettings: {}
            };

            function mockGetAppSettings() {
                var appSettings = {
                    otherwise: "home",
                    theme: 'cyan',
                    menu: [],
                    files: {
                        someOtherJs: {
                            cache: true,
                            files: ['views/home/some.js?v=0']
                        }
                    },
                    routes: [
                        {
                            state: "other",
                            url: '/other',
                            templateUrl: 'views/other/other.html',
                            controller: 'otherController',
                            resolve: ['other']
                        }
                    ]
                };
                return $q.when(appSettings);
            }

            function initialize() {
                $$session.$$userRequest.promise.then(function (user) {
                    $scope.sv.user = user;
                    mockGetAppSettings().then(function (appSettings) {
                        $$session.appSettings = appSettings;
                        $scope.sv.appSettings = $$session.appSettings;
                        for (var config in appSettings.files) {
                            $$stateDictionary.scripts[config] = appSettings.files[config];
                        }

                        for (var index = 0, length = appSettings.routes.length, route = null; index < length; index++) {
                            route = appSettings.routes[index];
                            route.resolve = $$state.$resolve.apply(null, route.resolve);
                            $$state.state(route.state, route);
                        }

                        $$state.otherwise(appSettings.otherwise);
                    });

                    $(document).ready(function () {
                        // some style event could be here

                        $scope.$on("$stateChangeStart", function () {
                            // animations between routes could be here                            
                        });
                    });
                });
            }

            $scope.reload = function () {
                $$state2.reload();
            };

            initialize();
        }]);