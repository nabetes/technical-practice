﻿angular.module('markovPuzzle')
    .directive("wordPuzzle", ["$parse", "$http", "$$markovPuzzle", "$timeout",
        function ($parse, $http, $$markovPuzzle, $timeout) {

            function _lightenDarkenColor(color, percent) {
                var usePound = false;

                if (color[0] == "#") {
                    color = color.slice(1);
                    usePound = true;
                }

                var num = parseInt(color, 16);

                var r = (num >> 16) + percent;

                if (r > 255) r = 255;
                else if (r < 0) r = 0;

                var b = ((num >> 8) & 0x00FF) + percent;

                if (b > 255) b = 255;
                else if (b < 0) b = 0;

                var g = (num & 0x0000FF) + percent;

                if (g > 255) g = 255;
                else if (g < 0) g = 0;

                return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
            }

            $(document).on("mouseover.puzzle-word", ".puzzle-word", function () {
                var element = $(this),
                    model = element.data("$scope").item;
                $(model.selector).css({
                    "background-color": _lightenDarkenColor(model.color, - 75),
                    "font-weight": 600,
                    color: "white"
                });
            });

            $(document).on("mouseleave.puzzle-word", ".puzzle-word", function () {
                var element = $(this),
                    model = element.data("$scope").item;
                $(model.selector).css({
                    "background-color": model.color,
                    "font-weight": 200,
                    color: "black"
                });
            });

            return {
                restrict: 'A',
                scope: {
                    puzzleMatrix: "=",
                    puzzleWords: "@"
                },
                transclude: false,
                replace: true,
                template: '<table> <tr ng-repeat="row in puzzleMatrix" class="matrix-row"> <td ng-repeat="item in row" ng-bind="item.v" class="matrix-cell" id="{{item.id}}"></td> </tr> </table>',
                link: function ($scope, $element, $attrs, $ctrl, $transclude) {

                    function puzzle() {
                        this.words = [];
                        if ($attrs.wordPuzzle) {
                            $parse($attrs.wordPuzzle).assign($scope.$parent, this);
                        }
                    }

                    puzzle.prototype.findWords = function () {
                        var $this = this;
                        return $http({
                            method: 'GET',
                            url: $$markovPuzzle.config.urlBase + "api/markov/getWords"
                        }).then(function (request) {
                            $this.words = request.data;
                            return $this._animateFindings();
                        });
                    }

                    puzzle.prototype._animateFindings = function (words, newWords) {
                        var $this = this,
                            _words = [].concat(words || this.words),
                            newWords = newWords || [],
                            currentWord = null;

                        if (_words.length) {
                            currentWord = _words.pop();
                            currentWord.color = $this._getRandColor();
                            currentWord.selector = "";
                            newWords.push(currentWord);

                            if ($attrs.puzzleWords) {
                                $parse($attrs.puzzleWords).assign($scope.$parent, newWords);
                            }

                            currentWord.breakdown.forEach(function (item, index2) {
                                currentWord.selector += "#{0}-{1}, ".$$stringFormat(item.row, item.column);
                            });

                            currentWord.selector = currentWord.selector.substring(0, currentWord.selector.length - 2);
                            $this._updatetyle(currentWord, _words, newWords);
                        }
                    };

                    puzzle.prototype._updatetyle = function (currentWord, _words, newWords) {
                        var $this = this;
                        $timeout(function () {
                            $(currentWord.selector).css("background-color", currentWord.color);
                            $timeout(function () {
                                $this._animateFindings(_words, newWords);
                            }, 300);
                        }, 0);
                    };

                    puzzle.prototype._getRandColor = function (brightness) {
                        var letters = 'BCDEF'.split('');
                        var color = '#';
                        for (var i = 0; i < 6; i++) {
                            color += letters[Math.floor(Math.random() * 4)];
                        }
                        return _lightenDarkenColor(color, -12);
                    };

                    return new puzzle();
                }
            };
        }]);