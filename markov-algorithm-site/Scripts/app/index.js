﻿'use strict';
angular.module("markovPuzzle")
    .controller("indexController", ["$scope", "$$session", "$window", "$q", "$$state2", "$$stateDictionary", "$http", "$$state2",
        function ($scope, $$session, $window, $q, $$state, $$stateDictionary, $http, $$state2) {

            $scope.sv = {
                user: null,
                appSettings: {}
            };

            function mockGetAppSettings() {
                var appSettings = {
                    otherwise: "home",
                    theme: 'cyan',
                    menu: [],
                    files: {
                        someOtherJs: {
                            cache: true,
                            files: ['views/home/some.js?v=0']
                        }
                    },
                    routes: [
                        {
                            state: "other",
                            url: '/other',
                            templateUrl: 'views/other/other.html',
                            controller: 'otherController',
                            resolve: ['other']
                        }
                    ]
                };
                return $q.when(appSettings);
            }

            function initialize() {
                $$session.$$userRequest.promise.then(function (user) {
                    $scope.sv.user = user;
                    mockGetAppSettings().then(function (appSettings) {
                        $$session.appSettings = appSettings;
                        $scope.sv.appSettings = $$session.appSettings;
                        for (var config in appSettings.files) {
                            $$stateDictionary.scripts[config] = appSettings.files[config];
                        }

                        for (var index = 0, length = appSettings.routes.length, route = null; index < length; index++) {
                            route = appSettings.routes[index];
                            route.resolve = $$state.$resolve.apply(null, route.resolve);
                            $$state.state(route.state, route);
                        }

                        $$state.otherwise(appSettings.otherwise);
                    });

                    $(document).ready(function () {
                        // some style event could be here

                        $scope.$on("$stateChangeStart", function () {
                            // animations between routes could be here                            
                        });
                    });
                });
            }

            $scope.reload = function () {
                $$state2.reload();
            };

            initialize();
        }]);
