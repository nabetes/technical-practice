﻿'use strict';
angular.module('markovPuzzle', ['ui.router', 'oc.lazyLoad', 'ngSanitize', '$$exception', '$$modal', "appRouteConfig"])
    .config(['$ocLazyLoadProvider', "$locationProvider", function ($ocLazyLoadProvider, $locationProvider) {
        $locationProvider.hashPrefix('');

        $ocLazyLoadProvider.config({
            debug: false,
            events: true,
            modules: []
        });
    }]);