﻿'use scrict';
angular.module("appRouteConfig", ["$$http", "$$session", "$$state", "$$sessionTest"])
    .config(["$$markovPuzzleProvider", function ($$markovPuzzleProvider) {
        $$markovPuzzleProvider.setConfig({
            urlBase: "http://localhost:60059/"
        });
    }])
    .run(["$$session", "$$state2", "$$stateDictionary", "$window", "$q", "$$exception", "$$http", "$$markovPuzzle",
        function ($$session, $$state, $$stateDictionary, $window, $q, $$exception, $$http, $$markovPuzzle) {
            $$session.user = null;
            $$session.$$userRequest = $q.defer();
            $$session._setKey("markovPuzzle");

            $$exception.onError = function (error) {
                //check if is controlled exception from de web api, logOut
                $$exception.show(error);
            }

            $$http.onRequest = function () {
                //  if the request if for the the web api, we should add the current token.
                var args = arguments,
                    $delegate = args[0],
                    firstParameter = null;
                Array.prototype.shift.call(args);
                firstParameter = args[0];

                // Having some problems with angular $http headers to oAuth
                /*
                 * If the request its go to markov-algorithm-api, this section should include the validation token,
                 * that .net needs to validate the access to the differents controllers.
                 * 
                 * By now the "authorize attribute" of the controllers are not included,
                 * security wasn't included on the requirements, so because of time I'll escape this part
                 */
       
                return $delegate.apply(null, [].slice.call(args));
            }

            $$session.initialize().then(function (request) {
                if ($window.location.pathname === "/login.html")
                    return $window.location.href = window.location.origin;

                $$stateDictionary.scripts.home = {
                    cache: true,
                    files: ['views/home/home.js?v=0']
                };

                $$state.$resolve = function () {
                    var $argumengs = arguments,
                        $base = $$state.resolve.apply($$state, $$.toArray($argumengs));

                    $base["user"] = ["$q", "$timeout", function ($q, $timeout) {
                        return $$session.user ?
                            $q.when($$session.user) : $$session.getUser().then(function (user) {
                                $$session.user = user;
                                if ($$session.$$userRequest.promise.$$state.status === 0) {
                                    $$session.$$userRequest.resolve(user);
                                }
                                return $$session.user;
                            });
                    }];

                    return $base;
                };

                $$state.otherwise('home');
                $$state.state("home", {
                    url: '/home',
                    templateUrl: 'views/home/home.html',
                    controller: 'homeController',
                    resolve: $$state.$resolve('home')
                });
                $$state.go("home");
            }, function () {
                if ($window.location.pathname !== "/login.html")
                    $window.location.href = window.location.origin + "/login.html";
            });
        }]);