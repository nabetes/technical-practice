﻿'use scrict';
angular.module('$$sessionTest', ['$$session', 'oc.lazyLoad'])
    .provider("$$markovPuzzle", [function () {
        var $this = this;
        this.config = {
            urlBase: "",
            tokenKey: "$$sessionTest"
        };
        this.setConfig = function (config) {
            Object.assign($this.config, config);
        }
        this.$get = function () {
            return {
                config: $this.config
            }
        }
    }])
    .run(["$$sessionManagerDictionary", "$$markovPuzzle", "$ocLazyLoad", "$q", "$http", "$window", function ($$sessionManagerDictionary, $$markovPuzzle, $ocLazyLoad, $q, $http, $window) {
        $$markovPuzzle.getToken = getToken;

        function getToken() {
            return $window.localStorage.getItem($$markovPuzzle.config.tokenKey);
        }

        function setToken(value) {
            $window.localStorage.setItem($$markovPuzzle.config.tokenKey, value);
        }

        var media = {
            key: "markovPuzzle",
            login: function () {
                if (getToken()) {

                    return $q.when(1);
                }
                return this.checkStatus();
            },
            logOut: function () {
                setToken("");
                return $.when(1);
            },
            getUser: function () {
                var token = getToken();
                if (token) {
                    var base64Url = token.split('.')[1];
                    var base64 = base64Url.replace('-', '+').replace('_', '/');
                    return $q.when(JSON.parse(window.atob(base64)));
                }

                return $q.reject(null);
            },
            checkStatus: function () {
                if (getToken()) {
                    return $q.when(1);
                }

                /*In this implementation the "check status" method of this session manager, 
                 * already have get the token of validation, so thats why dont use the login method in any place of the app.
                 */
                return $http.get($$markovPuzzle.config.urlBase + "api/security/GetToken")
                    .then(function (request) {
                        setToken(request.data);
                        return request;
                    });
            },
            initialize: function () {
                return $q.when(1);
            }
        };
        $$sessionManagerDictionary.markovPuzzle = media;
    }]);