﻿'use strict';
angular.module('appSessionModule', [])
    .provider('$$session', [function () {
        var currentUserKey = "appUser";
        var $this = this;
        this.appName = "home";
        this.$get = ['$q', '$timeout', "$window", function ($q, $timeout, $window) {
            function validateUser() {
                var currentUser = null;
                currentUser = $window.localStorage.getItem(currentUserKey);
                if (true) {
                    return loadHome();
                }

                if ($this.appName != "login") {
                    location.href = window.location.origin + "/login.html";
                }
            }

            function loadHome() {
                // logic associated with seccion initialization could be here.
            }

            return {
                validate: function () {
                    var mockPromise = $q.defer();
                    $timeout(function () {
                        validateUser();
                        mockPromise.resolve({ name: "johnny" });
                    }, 0);
                    return mockPromise.promise;
                }
            }
        }]
    }]);
