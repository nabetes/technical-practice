﻿'use strict';
angular.module('$$state', ['ui.router', 'oc.lazyLoad'])
    .provider('$$stateDictionary', function () {
        this.$get = function () {
            return {
                scripts: {},
                modules: {}
            };
        };
    })
    .provider('$$state2', ["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
        var $arguments = [];
        this.$get = ['$ocLazyLoad', '$q', "$$stateDictionary", "$state", function ($ocLazyLoad, $q, $$stateDictionary, $state) {
            return {
                resolve: function () {
                    var $arguments = arguments;
                    return {
                        _modulesAndScripts: function () {
                            var requestsCollection = [];
                            for (var index = 0, length = $arguments.length, config = null, request = null; index < length; index++) {
                                config = $arguments[index];
                                if (typeof config === "string") {
                                    request = $ocLazyLoad.load($$stateDictionary.scripts[config] || $$stateDictionary.modules[config]);
                                } else {
                                    request = $ocLazyLoad.load(config);
                                }
                                requestsCollection.push(request);
                            }
                            return $q.all(requestsCollection);
                        }
                    };
                },
                reload: function () {
                    return $state.reload();
                },
                state: function (name, config) {
                    return $stateProvider.state(name, config);
                },
                otherwise: function (name) {
                    return $urlRouterProvider.otherwise(name);
                },
                go: function () {
                    return $state.go.apply($state, $$.toArray(arguments));
                },
                get: function () {
                    return $state.get.apply($state, $$.toArray(arguments));
                }
            };
        }];
    }]);