﻿"use strict";
angular.module('$$session', [])
    .provider('$$sessionManagerDictionary', function () {
        var dictionary = {};

        this.addMedia = function (key, value) {
            dictionary[key] = value;
        }

        this.$get = function () {
            return dictionary;
        };
    })
    .provider("$$session", [function () {
        var $this = this;
        $this.currentMedia = null;

        $this.$get = ["$q", "$$sessionManagerDictionary", "$window", function ($q, $$sessionManagerDictionary, $window) {
            function onReady() {
                if ($this.currentMedia._promise.$$state.status === 1) {
                    return $q.when(1);
                }

                return $this.currentMedia._promise;
            }

            function setKey(key) {
                $window.localStorage.setItem("$$sessionKey", key || $this.currentMedia.key);
            }

            function getKey() {
                return $window.localStorage.getItem("$$sessionKey");
            }

            var $socialService = {
                _setKey: function (key) {
                    setKey(key);
                },
                _getKey: function (key) {
                    getKey();
                },
                login: function (key, config) {
                    $this.currentMedia = $$sessionManagerDictionary[key];
                    setKey();
                    return onReady().then($this.currentMedia.checkStatus)
                        .then(function (response) {
                            return response;
                        }, function () {
                            return $this.currentMedia.login();
                        });
                },
                logOut: function () {
                    if ($this.currentMedia) {
                        return $this.currentMedia.logOut();
                    }
                    return $q.when(1);
                },
                checkStatus: function (key) {
                    $this.currentMedia = $$sessionManagerDictionary[key];
                    setKey();
                    return $this.currentMedia.checkStatus();
                },
                getUser: function () {
                    return $this.currentMedia.getUser();
                },
                initialize: function () {
                    var statusRequest = null,
                        key = getKey();

                    if (key) {
                        $this.currentMedia = $$sessionManagerDictionary[key];
                        $this.currentMedia._promise = $this.currentMedia.initialize()
                            .then($this.currentMedia.checkStatus);

                        statusRequest = $this.currentMedia._promise;
                    } else {
                        statusRequest = $q.when(1);
                    }

                    return statusRequest.then(function (request) {
                        return request;
                    }, function (request) {
                        var $access = null;
                        for (var access in $$sessionManagerDictionary) {
                            $access = $$sessionManagerDictionary[access];
                            if ($access.key !== key) {
                                $access._promise = $access.initialize();
                            }
                        }
                        return $q.reject(request);
                    });
                }
            };
            return $socialService;
        }];
    }]);