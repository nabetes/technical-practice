﻿'use strict';
angular.module('$$exception', [])// only apply for controller         
    .factory("$$exception", ["$rootScope", "$document", "$compile", function ($rootScope, $document, $compile) {
        var modalTemplate = '<div id="SSexception-modal" $$modal="sv.modal" $$title="sv.title"> <div class="modal-body SSexception-modal-list" ng-bind-html="sv.msgExceptions"></div> <footer class="modal-footer"> <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal" ng-bind="sv.labelAccept"> </button> </footer> </div>';
        var genericErrorMessage = "Something unexpected happened, contact the support manager.";
        var $scope = $rootScope.$new();
        window.$$exception = $$exception;
        window.$scope = $scope;

        $scope.sv = {
            msgExceptions: "",
            labelAccept: "Accept",
            title: "System message!",
            modal: null
        };

        /**
        * Constructor of the class
        * @param error: error
        */
        function $$exception() {
            this.Exceptions = [];
            this.message = genericErrorMessage;

            if (arguments.length) {
                $$exception.prototype.add.apply(this, arguments);
            }
        };

        /*
        * Execute when an exception appear
        */
        $$exception.onError = null;

        /**
        * Add new exceptions to the colecction
        * @param error: error
        */
        $$exception.prototype.add = function () {
            var $this = this,
                error = arguments[0];
            switch (typeof error) {
                case "string":
                    if (error.indexOf("ExceptionList") != -1) {
                        var request = JSON.parse(error);
                        $this.message = request.Message;
                        $this.add(request.ExceptionList);
                    } else {
                        if (arguments.length > 1) {
                            Array.prototype.shift.apply(arguments);
                            error = String.prototype.$$stringFormat.apply(error, arguments);
                        } else {
                            $this.message = error;
                            error = genericErrorMessage;
                        }
                        $this._addStringError(error);
                    }
                    break;
                case "function":
                    $this.add(error("$$error"));
                    break;
                case "object":
                    if (error.constructor === $$exception) {
                        $this.Exceptions = $this.Exceptions.concat(error.Exceptions);
                    } else {
                        if (error.message) {
                            $this.message = error.message;
                            $this._addStringError(genericErrorMessage);
                        } else if ($.isArray(error)) {
                            angular.forEach(error, function (message) {
                                $this._addStringError(message);
                            });
                        } else {
                            $this.message = JSON.stringify(error);
                            $this._addStringError(genericErrorMessage);
                        }
                    }
                    break;
            }
            return this;
        };

        /**
         * Add a string exception
         */
        $$exception.prototype._addStringError = function (error) {
            this.Exceptions.push('<span class="SSexception-item"> <span class="SSexception-item-icon zmdi zmdi-alert-circle zmdi-hc-fw"></span>{0}</span>'.$$stringFormat(error));
        }

        /**
        * Check if the handler have exceptions
        */
        $$exception.prototype.validate = function () {
            if (this.Exceptions.length) {
                this.throw();
            }
        };

        /**
        * Add and throw a new exception
        * @param error: error
        */
        $$exception.prototype.addAndThrow = function () {
            $$exception.prototype.add.apply(this, arguments);
            this.throw();
        };

        /**
        * Clean the exceptions
        */
        $$exception.prototype.clean = function () {
            this.Exceptions = [];
        };

        /**
         * throw the current handler
         */
        $$exception.prototype.throw = function () {
            throw this;
        };

        /* Show an exception
        * @param {generic} exception 
        */
        $$exception.prototype.show = function () {
            if (!$scope.sv.modal) {
                $document.find("body:first").append($compile(modalTemplate)($scope));
            }

            if (arguments.length) {
                $$exception.prototype.add.apply(this, arguments);
            }
            $scope.sv.msgExceptions = this.Exceptions.join("");
            this.Exceptions = [];
            if (!$scope.$root.$$phase) {
                $scope.$root.$digest();
            }
            $scope.sv.modal.open();
        }

        /**
         * Add and throw
         * @param {object} parametros del error
         */
        $$exception.addAndThrow = function () {
            $$exception.prototype.addAndThrow.apply(new $$exception(), arguments);
        };

        /* Show an exception
        * @param {generic} exception 
        */
        $$exception.show = function () {
            var exception = arguments[0];
            if (exception.constructor !== $$exception) {
                if (arguments.length > 1) {
                    exception = new $$exception();
                    exception.add.apply(exception, arguments);
                } else {
                    exception = new $$exception(exception);
                }
            }
            exception.show();
        };

        return $$exception;
    }])
    .config(["$provide", "$$exceptionProvider", function ($provide, $exceptionProvider) {
        $provide.decorator('$exceptionHandler', ["$delegate", "$injector", function ($delegate, $injector) {
            var $$exception = null;
            return function (error, cause) {
                if (!$$exception) {
                    $$exception = $injector.get("$$exception");
                }
                $delegate(error, cause);//default error handling
                if ($$exception.onError)
                    return $$exception.onError(error);

                $$exception.show(error);//custom error
            };
        }]);
    }]);