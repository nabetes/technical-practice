﻿angular.module('$$modal', [])
    .directive("$$modal", ["$parse", function ($parse) {
        return {
            restrict: 'A',
            scope: {
                $$modal: "@",
                $$title: "=?",
                $$size: "@?",
                $$onOpen: "&?"
            },
            transclude: {
                '$$header': '?header',
                '$$footer': '?footer'
            },
            template: '<div id="test1" class="modal fade" tabindex="-1" style="margin-right:10px"> <div class="modal-dialog"> <div class="modal-content"> <ng-transclude ng-if="$$header" ng-transclude-slot="$$header"> </ng-transclude> <div ng-if="$$title" class="modal-header"> <h5 class="modal-title pull-left" ng-bind="$$title"></h5> </div> <ng-transclude> </ng-transclude> <ng-transclude ng-if="$$footer" ng-transclude-slot="$$footer"> </ng-transclude> </div> </div> </div>',
            link: function ($scope, $element, $attrs, $ctrl, $transclude) {
                $scope.$$header = $transclude.isSlotFilled('$$header');
                $scope.$$footer = $transclude.isSlotFilled('$$footer');

                $scope.$$size = $scope.$$size || 'default';
                $scope.$$title = $scope.$$title || 'title';                

                var element = $($element[0].childNodes[0]);

                function $$modal() {
                    if ($attrs.$$modal) {
                        $parse($attrs.$$modal).assign($scope.$parent, this);
                    }
                }

                $$modal.prototype.open = function () {
                    if ($scope.$$onOpen) {
                        $scope.$$onOpen();
                    }
                    if (!$scope.$root.$$phase) {
                        $scope.$digest();
                    }
                    element.modal('show');
                }

                $$modal.prototype.close = function () {
                    element.modal('hide');
                }

                return new $$modal();
            }
        };
    }]);