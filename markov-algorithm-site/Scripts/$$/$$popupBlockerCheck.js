﻿angular.module("$$popupBlockerCheckModule", [])
    .factory("$$popupBlockerCheck", [function () {
        return function checkBlocker() {
            var openWin = window.open("http://www.google.com", "directories=no,height=100,width=100,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
            if (!openWin) {
                return alert("A popup blocker was detected. Please Remove popupblocker for this site");
            }

            return openWin.close();
        }
    }]);