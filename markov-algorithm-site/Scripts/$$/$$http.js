﻿'use strict';
angular.module('$$http', [])
    .factory("$$http", [function () {
        return {
            onRequest: null
        }
    }])
    .config(["$provide", function ($provide) {
        var $$http = null;
        $provide.decorator('$http', ["$delegate", "$injector", function ($delegate, $injector) {
            var $get = $delegate.get,
                $post = $delegate.post;

            if (!$$http) {
                $$http = $injector.get("$$http");
            }

            function execute(args, task) {
                if ($$http.onRequest) {
                    return $$http.onRequest.apply($delegate, [task].concat(args));
                }

                return task.apply($delegate, args);
            }

            $delegate.get = function () {
                return execute([].slice.call(arguments), $get);
            };

            $delegate.post = function () {
                return execute([].slice.call(arguments), $post);
            };

            return $delegate;
        }]);
    }]);