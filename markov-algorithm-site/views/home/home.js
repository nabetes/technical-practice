﻿angular.module("markovPuzzle")
    .controller("homeController", ["$scope", "$http", "$$markovPuzzle", "$timeout",
        function ($scope, $http, $$markovPuzzle, $timeout) {

            $scope.sv = {
                flagLoadingPuzzle: true,
                flagLoadingWords: false,
                flagShowWords: false,

                matrix: [],
                words: [],

                puzzle: null
            };

            function Initialize() {
                window.$http = $http;

                $http({
                    method: 'GET',
                    url: $$markovPuzzle.config.urlBase + "api/markov/getMatrix"
                }).then(function (request) {
                    var newMatrix = [];
                    for (var index = 0, length = request.data.length, tempArray = []; index < length; index++) {
                        tempArray = request.data[index].split('');
                        newMatrix.push(tempArray.map(function (x, subIndex) {
                            return {
                                v: x,
                                id: "{0}-{1}".$$stringFormat(index, subIndex)
                            };
                        }));
                    }

                    $scope.sv.matrix = newMatrix;
                }).then(function () {
                    $scope.sv.flagLoadingPuzzle = false;
                });
            }

            $scope.findWords = function () {
                $scope.sv.flagLoadingWords = true;
                $scope.sv.flagShowWords = true;
                $scope.sv.puzzle.findWords().then(function () {
                    $timeout(function () {
                        $scope.sv.flagLoadingWords = false;
                    }, 1500);
                });
            };

            Initialize();
        }]);